const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
		username:{
			type: String
		},
		firstName: {
			type: String,
			required: [true, `First name is required`]
		},

		lastName: {
			type: String,
			required: [true, `Last name is required`]
		},
		email: {
		type: String,
		required: [true, `Email required`],
		unique: true
		},
		password: {
			type: String,
			required: [true, `Pasword is required`]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		orders: [
			
			{
			// productId: {
			// 	type: String,
			// 	required: [true, `Product ID is required`]
			// },
			
			orderId: {
				type: String,
				required: [true, `Order ID is required`]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
}, {timestamps: true})

module.exports = mongoose.model(`User`, userSchema);

